# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 11:40:18 2021

@author: yourt
"""
from SONATA.classBlade import Blade

job = Blade(name = "mars helicopter ingenuity", filename='ingenuity.yml', ref_axes_format=0, ontology=0)
    
job.blade_gen_section(mesh_flag = True, split_quads=True)
job.blade_plot_sections()
#job.blade_post_3dtopo(flag_wf = True, flag_lft = True, flag_topo = True, flag_mesh = False, flag_stp=False)
#job.blade_run_anba()
#job.blade_run_vabs()
#job.blade_exp_dymore_inpt(eta_offset=0.194)
