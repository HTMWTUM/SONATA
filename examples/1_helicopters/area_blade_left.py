# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 11:40:18 2021

@author: yourt
"""
import numpy as np
from SONATA.classBlade import Blade


def area_twist(r):
    return -(r-0.75)*np.deg2rad(10)
    

job = Blade(name = "area_blade", filename='area_blade_left.yml', ref_axes_format=0, ontology=0)
    
job.blade_gen_section(mesh_flag = True, split_quads=False)
job.blade_plot_sections()
job.blade_post_3dtopo(flag_wf = False, flag_lft = False, flag_topo = True, flag_mesh = False, flag_stp=True)


twist = {}
twist["grid"] = np.linspace((250/1650),1,10)
twist["values"] = area_twist(twist["grid"])