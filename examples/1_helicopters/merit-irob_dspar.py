#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as tri
from SONATA.classBlade import Blade
from SONATA.cbm.mesh.cell_utils import calc_cell_angles
from SONATA.cbm.mesh.mesh_utils import (grab_nodes_of_cells_on_BSplineLst,
                                        merge_nodes_if_too_close,
                                        sort_and_reassignID,)

job = Blade(name = "Test", filename='merit-irob_dspar.yml', ref_axes_format=0, ontology=0)
job.blade_gen_section(mesh_flag = True, split_quads=True)
job.blade_plot_sections()
#job.blade_gen_loft(ruled=True, tolerance=1e-6, continuity=4, check_compatibility=True, filename="simple.iges")
job.blade_post_3dtopo(flag_lft = True, flag_topo = True, flag_mesh = False)

loads = { "F" : np.array([[0, 0, 0, 0], 
                         [1, 0, 0, 0]]),
          "M" : np.array([[0, 0, 300, 0],
                         [1, 0, 300, 0]])}
 
#job.blade_run_anba(loads)
#job.blade_plot_sections(attribute = "strain.epsilon13", plotDisplacement=True, u_scale_factor = 10)
#job.blade_plot_sections(attribute = "strainM.epsilon11")
#job.blade_plot_sections(attribute = "stress.sigma13")