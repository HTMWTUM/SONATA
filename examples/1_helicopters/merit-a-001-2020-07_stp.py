# -*- coding: utf-8 -*-
"""
Created on Mon Mar 15 11:40:18 2021

@author: yourt
"""
from SONATA.classBlade import Blade

job = Blade(name = "merit_blade", filename='merit-a-001-2020-07_stp.yml', ref_axes_format=0, ontology=0)
    
job.blade_gen_section(mesh_flag = True, split_quads=True)
#job.blade_plot_sections()
#job.blade_post_3dtopo(flag_wf = False, flag_lft = False, flag_topo = True, flag_mesh = False, flag_stp=True)
#job.blade_run_anba()
job.blade_run_vabs()
#job.blade_exp_dymore_inpt(eta_offset=0.194)
