# -*- coding: utf-8 -*-
"""
Created on Mo Oct 07 11:18:28 2019

@author: Roland Feil
"""

import os, sys
sys.path.insert(0,'/Users/rfeil/work/6_SONATA_new/SONATA')  # import sys path to import 'SONATA' & 'job' modules

import time
import subprocess
import matplotlib.pyplot as plt
import numpy as np
import csv
# from openmdao.api import ScipyOptimizer
from openmdao.api import Group, Problem, IndepVarComp, ScipyOptimizeDriver, ExplicitComponent, ExecComp  #, pyOptSparseDriver
from openmdao.drivers.genetic_algorithm_driver import SimpleGADriver
# from concurrent import futures
from openmdao.drivers.pyoptsparse_driver import pyOptSparseDriver

from SONATA.classBlade import Blade
from SONATA.utl_openfast.utl_sonata2beamdyn import convert_structdef_SONATA_to_beamdyn


print('Current working directory is:', os.getcwd())



class StructOpt(ExecComp):

    def setup(self):

        # inputs
        self.add_input('OptVar1')
        self.add_input('OptVar2')
        self.add_input('OptVar3')
        self.add_input('OptVar4')
        self.add_input('OptVar5')
        # outputs
        self.add_output('Stiffness_obj')

    def compute(self, inputs, outputs):
        OptVar = [inputs['OptVar1'], inputs['OptVar2'], inputs['OptVar3'], inputs['OptVar4'], inputs['OptVar5']]  # to-be-varied variable
        print('NEW OptVar1 = ' + str(float(inputs['OptVar1'])))
        print('NEW OptVar2 = ' + str(float(inputs['OptVar2'])))
        print('NEW OptVar3 = ' + str(float(inputs['OptVar3'])))
        print('NEW OptVar4 = ' + str(float(inputs['OptVar4'])))
        print('NEW OptVar5 = ' + str(float(inputs['OptVar5'])))

        # run job
        ontology = 0
        ref_axes_format = 0
        job = Blade(name=job_name, filename=filename_str, ontology=ontology, ref_axes_format=ref_axes_format, stations=radial_stations, flag_opt=flag_opt, opt_vars=OptVar)
        job.blade_gen_section(topo_flag=True, mesh_flag=True, split_quads=True, mesh_resolution=200)

        # job.blade_run_vabs(vabs_path=vabs_path)
        job.blade_run_anba()

        beam_prop = job.beam_properties
        beam_prop_MM = beam_prop[0][1].MM
        beam_prop_TS = beam_prop[0][1].TS

        # outputs['EIflap'] = beam_prop['beam_stiff'][0,4,4]
        GJ_diff = np.abs(beam_prop_TS[3, 3] - beam_prop_init_TS[3, 3])/beam_prop_init_TS[3, 3]
        EIflap_diff = np.abs(beam_prop_TS[4, 4] - beam_prop_init_TS[4, 4])/beam_prop_init_TS[4, 4]
        EIlag_diff = np.abs(beam_prop_TS[5, 5] - beam_prop_init_TS[5, 5])/(beam_prop_init_TS[5, 5])
        mass_fraction = beam_prop_MM[0][0]/beam_prop_init_MM[0][0]

        Stiffness_diff_total = np.sqrt(GJ_diff**2 + EIflap_diff**2 + EIlag_diff**2)
        outputs['Stiffness_obj'] = mass_fraction + np.sqrt(GJ_diff**2 + EIflap_diff**2 + EIlag_diff**2)  # <<<

        print('*******************')
        print('STIFFNESS:')
        print('GJ = ' + str(beam_prop_TS[3, 3]))  # GJ
        print('EIflap = ' + str(beam_prop_TS[4, 4]))  # EIFLAP
        print('EIlag = ' + str(beam_prop_TS[5, 5]))  # EILAG
        print('*******************')
        print('OptVar1 = ' + str(float(OptVar[0])))
        print('OptVar2 = ' + str(float(OptVar[1])))
        print('OptVar3 = ' + str(float(OptVar[2])))
        print('OptVar4 = ' + str(float(OptVar[3])))
        print('OptVar5 = ' + str(float(OptVar[4])))
        print('Delta_GJ = ' + str(float(GJ_diff)))
        print('Delta_EIflap = ' + str(float(EIflap_diff)))
        print('Delta_EIlag = ' + str(float(EIlag_diff)))
        print('Stiffness OBJECTIVE = ' + str(float(outputs['Stiffness_obj'])))
        print('*******************')

        # job.blade_plot_sections(attribute='MatID', plotTheta11=False, plotDisplacement=False, savepath=folder_str, opt_var=str(float(OptVar[0])))

        # write result of each iteration to file
        with open('opt_temp.csv', 'a') as opt_csvfile:
            opt_csvfile_writer = csv.writer(opt_csvfile, delimiter=',')
                                        # OptVar1                   init_value                    iter_value              Objective - Delta
            opt_csvfile_writer.writerow([str(float(OptVar[0])), str(float(OptVar[1])), str(float(OptVar[2])), str(float(OptVar[3])), str(float(OptVar[4])),
                                         str(beam_prop_init_TS[3, 3]), str(beam_prop_TS[3, 3]), str(float(GJ_diff)),      # Torsion
                                         str(beam_prop_init_TS[4, 4]), str(beam_prop_TS[4, 4]), str(float(EIflap_diff)),  # Flap
                                         str(beam_prop_init_TS[5, 5]), str(beam_prop_TS[5, 5]), str(float(EIlag_diff)),   # Lag
                                         str(beam_prop_init_MM[0][0]), str(beam_prop_MM[0][0]), str(mass_fraction),
                                         str(Stiffness_diff_total),
                                         str(float(outputs['Stiffness_obj']))])


if __name__ == "__main__":

    start_time = time.time()

    folder_str = os.getcwd() + '/'
    job_name = 'SONATA_job_opt'

    job_str_init = 'BAR0020_ont5_baseline.yaml'  # Reference job for optimization objectives (yaml design)
    filename_str_init = folder_str + job_str_init

    job_str = 'BAR0020_ont0_3DprintSkin_BB_carbon_glasstriax_rR_0_60_TE.yaml'  # to-be-optimized job (yaml design)
    filename_str = folder_str + job_str

    vabs_path = "/Users/rfeil/work/8_VABS/vabs_WIN/AnalySwift/VABS/VABSIII.exe"  # not needed if anba4 is used

    # settings for reference job
    ontology = 5    # 0 - HT core structure, separated per cross section (SONATA/VABS ref axes as input)
                    # 1 - Shell - parameters distributed along the grid of a beam or blade span (to-be implemented) (SONATA/VABS ref axes as input)
                    # 2-3 - Dummy placeholder
                    # 5 - specific Wind Shell - parameters distributed along the grid of a beam or blade span with auto web splitting (WT axes & mat in vec format)
    ref_axes_format = 1  # 0 - "HT"; 1 - "WT"

    radial_stations = [0.60]  # provide to be optimized radial station of reference job

    # ============================= #
    # ===== Compute Reference ===== #
    # ============================= #
    # Initialize Reference job for scaling of optimization objectives
    job_init = Blade(name=job_name, filename=filename_str_init, ontology=ontology, ref_axes_format=ref_axes_format, stations=radial_stations)
    job_init.blade_gen_section(topo_flag=True, mesh_flag = True, split_quads=True, mesh_resolution = 200)
    # job_init.blade_run_vabs(vabs_path=vabs_path)
    job_init.blade_run_anba()
    # ============================= #

    # job_init.blade_plot_sections(attribute='MatID', plotTheta11=False, plotDisplacement=True, save_fig_filename=filename_str_init)  # plots reference blade cross section

    beam_prop_init = job_init.beam_properties  # uses SONATA/VABS coordinate system
    beam_prop_init_MM = beam_prop_init[0][1].MM
    beam_prop_init_TS = beam_prop_init[0][1].TS


    print('*******************')
    print('REFERENCE STIFFNESS:')
    print('GJ = ' + str(beam_prop_init_TS[3, 3]))  # GJ
    print('EIflap = ' + str(beam_prop_init_TS[4, 4]))  # EIFLAP
    print('EIlag = ' + str(beam_prop_init_TS[5, 5]))  # EILAG
    print('*******************')


    # ======================== #
    # ===== Optimization ===== #
    # ======================== #

    flag_opt = True         # activate optimization flag after model initialization
    try:
        os.remove('inflatable_blade_studies/opt_temp.csv')  # remove *.csv file before running another optimization
    except:
        print(' ')

    # set up Optimization Problem
    p = Problem()

    indeps = p.model.add_subsystem('indeps', IndepVarComp(), promotes=['OptVar1', 'OptVar2', 'OptVar3', 'OptVar4', 'OptVar5'])     # Three design variables
    indeps.add_output('OptVar1', 0.125257624969918)
    indeps.add_output('OptVar2', 0.0106352805813104)
    indeps.add_output('OptVar3', 0.001)
    indeps.add_output('OptVar4', 0.00330358065847527)
    indeps.add_output('OptVar5', 0.002)
    p.model.add_subsystem('structopt', StructOpt(), promotes_inputs=['OptVar1', 'OptVar2', 'OptVar3', 'OptVar4', 'OptVar5'], promotes_outputs=['Stiffness_obj'])

    # --------------------------------- #
    # SNOPT optimizer from pyOptSParse
    # --------------------------------- #
    p.driver = pyOptSparseDriver()  # ScipyOptimizeDriver()
    p.driver.options['optimizer'] = "SNOPT"
    p.driver.opt_settings['Major feasibility tolerance'] = 1e-6  #1e-7
    p.driver.opt_settings['Major optimality tolerance'] = 1e-3
    p.driver.opt_settings['Major iterations limit'] = 20
    p.driver.opt_settings['Summary file'] = 'SNOPT_Summary_file.txt'
    p.driver.opt_settings['Print file'] = 'SNOPT_Print_file.txt'
    p.driver.opt_settings['Major step limit'] = 0.1 # 1.0e-02


    # --------------------------------- #
    # Define Design Variables, Objecties and Constraints
    # --------------------------------- #
    # BAR0020 - r/R=0.6
    p.model.add_design_var('OptVar1', lower=0.08, upper=0.14)  # spar cap length
    p.model.add_design_var('OptVar2', lower=0.002, upper=0.012)   # or spar cap thickness
    p.model.add_design_var('OptVar3', lower=0.001, upper=0.04)   # or web thickness
    p.model.add_design_var('OptVar4', lower=0.001, upper=0.02)  # seg 3 thickness
    p.model.add_design_var('OptVar5', lower=0.001, upper=0.02)  # TE reinforcement thickness

    p.model.add_objective('Stiffness_obj', scaler = 1)

    p.setup()
    p.model.approx_totals(method='fd')
    p.run_driver()

    print("--- Computational time: %s seconds ---" % (time.time() - start_time))

    # # ===== PLOTS ===== #
    # job.blade_plot_sections(attribute=attribute_str, plotTheta11=flag_plotTheta11, plotDisplacement=False, savepath=folder_str)  # plots final optimized design
