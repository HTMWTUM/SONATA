# -*- coding: utf-8 -*-
"""
Created on Mo Oct 07 11:18:28 2019

@author: Roland Feil
"""

import os, sys
# sys.path.insert(0,'/Users/rfeil/work/6_SONATA_new/SONATA')  # import sys path to import 'SONATA' & 'job' modules

import time
import numpy as np

import matplotlib.pyplot as plt

# ==============
# Main
# ==============

start_time = time.time()
print('Current working directory is:', os.getcwd())

# 0.15
# t_max_ss = 0.25331980498680456  # max thickness arc location at suction side
# t_max_ps = 0.691610833984505  # max thickness arc location at pressure side

# 0.3
# t_max_ss = 0.32087714021701286  # max thickness arc location at suction side
# t_max_ps = 0.6683669437985169  # max thickness arc location at pressure side

# 0.45
# t_max_ss = 0.3356585184156316   # max thickness arc location at suction side
# t_max_ps = 0.6560479714239233    # max thickness arc location at pressure side

# 0.6
t_max_ss = 0.3400536973978655   # max thickness arc location at suction side
t_max_ps = 0.6569769632503524    # max thickness arc location at pressure side

# 0.75
# t_max_ss = 0.3392405192803253  # max thickness arc location at suction side
# t_max_ps = 0.6613964568209143  # max thickness arc location at pressure side

# 0.7645
# t_max_ss = 0.33763813598041315   # max thickness arc location at suction side
# t_max_ps = 0.6603507085422332    # max thickness arc location at pressure side

# 0.89
# t_max_ss = 0.3343765015135771  # max thickness arc location at suction side
# t_max_ps = 0.668561436174626  # max thickness arc location at pressure side



l_sc = 0.125257058534036  #0.139790491525893
# t_sc =
t_web = 0.001  #0.00796845044491388

# t_seg3 =

web1_pos = [t_max_ss+l_sc, t_max_ps-l_sc]
web2_pos = [t_max_ss+l_sc-t_web, t_max_ps-l_sc+t_web]
web3_pos = [t_max_ss-l_sc+t_web, t_max_ps+l_sc-t_web]
web4_pos = [t_max_ss-l_sc, t_max_ps+l_sc]

print(['Web1 start and end positions: ' + str(web1_pos)])
print(['Web2 start and end positions: ' + str(web2_pos)])
print(['Web3 start and end positions: ' + str(web3_pos)])
print(['Web4 start and end positions: ' + str(web4_pos)])

# print(['Spar cap thickness: ' + str(t_sc)])
# print(['Segment 3 thickness: ' + str(t_seg3)])


# print("--- Computational time: %s seconds ---" % (time.time() - start_time))
# EOF
