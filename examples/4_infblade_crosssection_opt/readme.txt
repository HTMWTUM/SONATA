Optimization example that optimizes various internal structural characteristics (layer locations, thickness, etc.) to match with a reference design.

Currently incorporates the SNOPT optimizer. SNOPT needs to be installed beforehand. Otherwise feel free to vary the optimizer, e.g. to ScipyOptimizeDriver()

Important to check that the correct gains are being optimized in utl_openmdao_apply_gains() that is located in SONATA/utl_openmdao/utl_openmdao

Optimization results are stored in the opt_temp.csv file. It follows the format displayed in the main script (see line 93 ff): (1) opt variables; (2) Torsion; (3) Flap; (4) Lag; (4) mass; (5) opt results. Note that (1) through (5) each contain multiple parameters, i.e. multiple rows in the *.csv file.
Use utls_inflatable_blades.py script for conversion to yaml inputs, e.g. of the web start and end locations along with the layer start and end locations of the box beam design!

Modify stiffness objective if need be; see line 70 in main script.


NOTE: Extended understanding of SONATA recommended!