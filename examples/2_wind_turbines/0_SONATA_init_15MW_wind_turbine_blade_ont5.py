# -*- coding: utf-8 -*-
"""
Created on Mo Oct 07 11:18:28 2019

@author: Roland Feil
"""

import os

import time
import numpy as np
from SONATA.classBlade import Blade

from SONATA.utl.recovery_utl import vabs_recovery_utl, anba_recovery_utl

# ==============
# Main
# ==============

start_time = time.time()

print('Current working directory is:', os.getcwd())


# ===== Provide Path Directory & Yaml Filename ===== #
folder_str = os.getcwd() + '/'
job_str = '0_ont5_IEA-15-240-RWT_TipShape_V3_mat.yaml'
job_name = '15MW_turbine_blade'

filename_str = folder_str + job_str


vabs_path = "/Users/rfeil/work/8_VABS/vabs_WIN/AnalySwift/VABS/VABSIII.exe"

# ===== SETTINGS ===== #
ontology                = 5         # 0 - HT core structure, separated per cross section (SONATA/VABS ref axes as input)
                                    # 1 - Shell - parameters distributed along the grid of a beam or blade span (to-be implemented) (SONATA/VABS ref axes as input)
                                    # 2 -
                                    # 5 - specific Wind Shell - parameters distributed along the grid of a beam or blade span with auto web splitting (WT axes & mat in vec format)

ref_axes_format         = 1         # 0 - "HT"; 1 - "WT"

radial_stations = [0.325]

job = Blade(name=job_name, filename=filename_str, ontology=ontology, ref_axes_format=ref_axes_format, stations=radial_stations)
job.blade_gen_section(topo_flag=True, mesh_flag = True, split_quads=True)

solver = "vabs"
# solver = "anba"


# ===== Recovery Analysis (inputs in SONATA coordinate system) ===== #
flag_recovery = True
if flag_recovery:
    # 15 MT turbine mean loads at 0.325 (test uses same loads for each radial station)
    Forces =  np.array([727300, -8737, 508643])  # forces, N (F1: axial force; F2,F3: sectional transverse shear forces)
    Moments =  np.array([-222054, -18047568, -226918])  # moments, Nm (M1: torsion, M2: flap; M3: lag)

    if solver == "vabs":
        loads = vabs_recovery_utl(Forces, Moments)
        job.blade_run_vabs(loads, vabs_path=vabs_path)
    elif solver == "anba":
        loads = anba_recovery_utl(Forces, Moments)
        job.blade_run_anba(loads)
else:
    if solver == "vabs":
        job.blade_run_vabs(vabs_path=vabs_path)
    elif solver == "anba":
        job.blade_run_anba()


# ===== PLOTS ===== #

# For plots within blade_plot_sections
attribute_str           = 'stress.sigma11'  # default: 'MatID' (theta_3 - fiber orientation angle)
                                    # others:  'theta_3' - fiber orientation angle
                                    #          'stress.sigma11' (use sigma_ij to address specific component)
                                    #          'stressM.sigma11'
                                    #          'strain.epsilon11' (use epsilon_ij to address specific component)
                                    #          'strainM.epsilon11'

# job.blade_plot_attributes(save_fig_filename=filename_str)
# job.blade_plot_beam_props(style="SONATA", save_fig_filename=filename_str)         # Define to be applied style: "SONATA", "DYMORE", "BEAMDYN" (default: "SONATA")
job.blade_plot_sections(attribute=attribute_str, plotTheta11=False, plotDisplacement=True, save_fig_filename=filename_str)
job.blade_post_3dtopo(flag_wf=True, flag_lft=True, flag_topo=True)

# ===== EXPORTS ===== #
# job.blade_csv_export_beam_props(style="SONATA", save_fig_filename=filename_str)   # Define to be applied style: "SONATA", "DYMORE", "BEAMDYN" (default: "SONATA")
# job.blade_export_analysis_files(style="BEAMDYN", stations=radial_stations, save_filename=filename_str, ontology=ontology)  # Define to be applied file format and definitions: "DYMORE" (to-be-added: "DYMORE", "CAMRAD II"); optional: unit_scaling='mm_to_m'

# EOF
