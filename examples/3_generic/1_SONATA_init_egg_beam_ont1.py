# -*- coding: utf-8 -*-
"""
Created on Mo Oct 07 11:18:28 2019

@author: Roland Feil
"""

import os
import time
import numpy as np
from SONATA.classBlade import Blade
from SONATA.utl.recovery_utl import vabs_recovery_utl, anba_recovery_utl


# ==============
# Main
# ==============

start_time = time.time()

print('Current working directory is:', os.getcwd())


# ===== Provide Path Directory & Yaml Filename ===== #
folder_str = os.getcwd() + '/'
job_str = '1_egg_beam_ont1.yaml'
job_name = 'egg_beam'

filename_str = folder_str + job_str


vabs_path = "/Users/rfeil/work/8_VABS/vabs_WIN/AnalySwift/VABS/VABSIII.exe"

# ===== SETTINGS ===== #
ontology                = 1         # 0 - HT core structure, separated per cross section (SONATA/VABS ref axes as input)
                                    # 1 - Shell - parameters distributed along the grid of a beam or blade span (to-be implemented) (SONATA/VABS ref axes as input)
                                    # 2 -
                                    # 5 - specific Wind Shell - parameters distributed along the grid of a beam or blade span with auto web splitting (WT axes & mat in vec format)

ref_axes_format         = 0         # 0 - "HT"; 1 - "WT"

radial_stations = [0.5]

job = Blade(name=job_name, filename=filename_str, ontology=ontology, ref_axes_format=ref_axes_format, stations=radial_stations)
job.blade_gen_section(topo_flag=True, mesh_flag = True, split_quads=True)

# solver = "vabs"
solver = "anba"
#

flag_recovery           = True
if flag_recovery:
    Forces =  np.array([0., 0., 0.])  # forces, N (F1: axial force; F2,F3: sectional transverse shear forces)
    Moments =  np.array([0., -1000000000000., 0.])  # moments, Nm (M1: torsion, M2: flap; M3: lag)    5000000

    if solver == "vabs":
        loads = vabs_recovery_utl(Forces, Moments)
        job.blade_run_vabs(loads, vabs_path=vabs_path)
    elif solver == "anba":
        loads = anba_recovery_utl(Forces, Moments)
        job.blade_run_anba(loads)
else:
    if solver == "vabs":
        job.blade_run_vabs(vabs_path=vabs_path)
    elif solver == "anba":
        job.blade_run_anba()

# ===== PLOTS ===== #
job.blade_plot_sections(attribute='strain.epsilon11', plotTheta11=True, plotDisplacement=True, save_fig_filename=filename_str)
job.blade_post_3dtopo(flag_wf=True, flag_lft=True, flag_topo=True)

# EOF
