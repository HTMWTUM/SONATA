#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 13, 2020

@author: RF
"""

import numpy as np
import csv


def csv_export_beam_properties(data, **kw):
    """
    Plots 6x6 mass and stiffness matrices.

    Parameters
    ----------
        data: structural properties

        data[:,0]   -   cs positions
        data[:, 1]  -   mass matrix
        data[:, 2]  -   stiffness matrix

        **kw : save_fig_filename

    Returns
    -------
        None.



    """


    # Export mass matrices for the defined radial stations
    with open(kw['save_fig_filename'][0:-5] + '_beam_properties_mass_matrices.csv', mode='w') as csv_file:
        beam_prop_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for i in range(len(data)):  # receive number of radial sections
            beam_prop_writer.writerow(' ')
            beam_prop_writer.writerow(['r/R', str(data[i][0])])

            for j in range(6):  # number of rows for each matrix
                beam_prop_writer.writerow(data[i,1][j,:])
    csv_file.close()



    # Export stiffness matrices for the defined radial stations
    with open(kw['save_fig_filename'][0:-5] + '_beam_properties_stiff_matrices.csv', mode='w') as csv_file:
        beam_prop_writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

        for i in range(len(data)):  # receive number of radial sections
            beam_prop_writer.writerow(' ')
            beam_prop_writer.writerow(['r/R', str(data[i][0])])

            for j in range(6):  # number of rows for each matrix
                beam_prop_writer.writerow(data[i,2][j,:])
    csv_file.close()



    return None