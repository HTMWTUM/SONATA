
# Third party modules
import numpy as np
import matplotlib.tri as tri
import matplotlib.pyplot as plt

try:
    import dolfin
    from anba4 import material
except ImportError as error:
    print(error.__class__.__name__ + ": " + error.message)
except Exception as exception:
    print(exception, False)


def trsf_s_to_a(v_s=None):
    """
    Defines Trsf Matrix from Sonata to Anba
    x1_anba = x2_sonata
    x2_anba = x3_sonata
    x3_anba = x1_sonata
    
    Parameters
    ----------
    v_s : np.array (optional)
        if an input argument is passed, the transformation is applied, else it 
        returns 
    
    Returns
    -------
    array : np.array of shape (3,3) that defines the Transformation Matrix so that
            v_a = np.dot(A,v_s), if the input argument is passed, it returns the 
            transformed instance
    """
    dir_x_a_s = np.array([0,1,0])
    dir_y_a_s = np.array([0,0,1])
    dir_z_a_s = np.array([1,0,0])
    A = np.vstack((dir_x_a_s, dir_y_a_s, dir_z_a_s))
    if isinstance(v_s, type(None)):
        return A
    else:
        return np.dot(A,v_s)


def build_mat_library(cbm_materials):
    """
    

    Parameters
    ----------
    cbm_materials : TYPE
        DESCRIPTION.

    Raises
    ------
    ValueError
        DESCRIPTION.

    Returns
    -------
    matLibrary : TYPE
        DESCRIPTION.
    matdict : TYPE
        DESCRIPTION.
    maxE : TYPE
        DESCRIPTION.

    """
    matLibrary = []

    maxE = 0.
    matid = 0
    matdict = {}
    for m in cbm_materials.values():
        if m.orth == 0:
            maxE = max(m.E, maxE)
            matMechanicProp = [m.E, m.nu]
            mat = material.IsotropicMaterial(matMechanicProp, m.rho)
        elif m.orth == 1:
            matMechanicProp = np.zeros((3,3))
            maxE = max(m.E[0], maxE)
            maxE = max(m.E[1], maxE)
            maxE = max(m.E[2], maxE)
            matMechanicProp[0,0] = m.E[1] #Exx
            matMechanicProp[0,1] = m.E[2] #Eyy
            matMechanicProp[0,2] = m.E[0] #Ezz
            matMechanicProp[1,0] = m.G[1] #g_yz
            matMechanicProp[1,1] = m.G[0] #g_xz
            matMechanicProp[1,2] = m.G[2] #g_xy
            matMechanicProp[2,0] = m.nu[1] #nu_zy
            matMechanicProp[2,1] = m.nu[0] #nu_zx
            matMechanicProp[2,2] = m.nu[2] #nu_xy
            mat = material.OrthotropicMaterial(matMechanicProp, m.rho)
        elif m.orth == 2:
            raise ValueError('material type 2 (anysotropic) not supported by Anba')
            
            #mat = material.
        matLibrary.append(mat)
        matdict[m.id] = matid
        matid = matid + 1
    return matLibrary, matdict, maxE


def build_dolfin_mesh(cbm_mesh, cbm_nodes, cbm_materials):
    """function to generate the dolfin.Mesh from a SONATA-CBM definition to run
    with anba

    Parameters
    ----------
    cbm_mesh : list of cell instances
        from the SONATA-CBM preprocessor

    cbm_nodes : list of nodes
        from the SONATA-CBM preprocessor


    Returns
    ----------
    mesh : dolfin.Mesh
    matLibrary : vector of anba materials
    materials : dolfin.MeshFunction definign cell materials
    plane_orientations : dolfin.MeshFunction defining cell plane orientations
    fiber_orientations : dolfin.MeshFunction defining cell material fiber orientation
    maxE : reference elastic modulus for scaling rigid mode constraints


    Notes
    ----------
    the cells of cbm_mesh already contain the nodes. So the information is 
    currently passed twice. But consistent with the export_cells_for_vabs.

    """
    (matLibrary, matdict, maxE) = build_mat_library(cbm_materials)

    mesh = dolfin.Mesh()
    me = dolfin.MeshEditor()
    me.open(mesh, "triangle", 2, 2)
    #me.open(mesh, "quadrilateral", 2,2)

    me.init_vertices(len(cbm_nodes))
    for n in cbm_nodes:
        me.add_vertex(n.id-1, n.coordinates)

    me.init_cells(len(cbm_mesh))
    for c in cbm_mesh:
        me.add_cell(c.id-1, [n.id-1 for n in c.nodes])

    me.close()

    materials = dolfin.MeshFunction("size_t", mesh, mesh.topology().dim())
    fiber_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
    plane_orientations = dolfin.MeshFunction("double", mesh, mesh.topology().dim())
    
    materials.set_all(0)
    plane_orientations.set_all(0.0)
    fiber_orientations.set_all(0.0)
    
    for c in cbm_mesh:
        materials[c.id-1] = matdict[c.MatID]
        plane_orientations[c.id-1] = c.theta_1[0]  # rotation around x1-axis (equiv. to beam axis) in SONATA/VABS coordinates; Theta_11
        fiber_orientations[c.id-1] = c.theta_3     # rotation around x3-axis in SONATA/VABS coordinates; Theta_3


    return mesh, matLibrary, materials, plane_orientations, fiber_orientations, maxE


def voight_to_2dtensor(v):
    arr = np.array([[v[0],v[5],v[4]],
                     [v[5],v[1],v[3]],
                     [v[4],v[3],v[2]]])
    return arr

def anba_recovery(anba, force, moment, voigt_convention="anba"):
    """
    Function to recover stresses and strains from an applied loading
    Results are generated in the global and local ('M' for material coordinate system) coordinates
    
    Parameters
    ----------
    anba    -   dolfin construct from anba
    n_el    -   number of mesh elements
    force   -   Forces in sonata coordinates, [F1, F2, F3], e.g. force = [2.2, 3.4, 1.1]
    moment  -   Moments in sonata coordinates, [M1, M2, M3], e.g. moment = [4.2, 5.7, 6.2]
    voigt_convention    -   "anba" with [s_xx, s_yy, s_zz, s_yz, s_xz, s_xy] or "paraview" with [s_xx, s_yy, s_zz, s_xy, s_yz, s_xz]
    T       -   Transformation matrix to convert results from ANBA to SONATA/VABS coordinates

    Returns
    ----------
    *_tran issues that outputs were converted to the SONATA/VABS coordinates
    remove '_tran' from the following output names when needed in ANBA coordinates

    tmp_StressF_tran    -   global stress field
    tmp_StressF_M_tran  -   local stress field
    tmp_StrainF_tran    -   global strain field
    tmp_StrainF_M_tran  -   local strain field

    Notes
    ------
    Author: RFeil, 
    revised by TPflumm

    # x1_anba = x2_sonata
    # x2_anba = x3_sonata
    # x3_anba = x1_sonata
    """

    nodes = anba.mesh.coordinates()
    elements = anba.mesh.cells()
    n_nodes = anba.mesh.num_vertices()
    n_el = len(elements)

    #Transform Forces and Moments from SONATA to ANBA Coordinates
    T = trsf_s_to_a()
    force = trsf_s_to_a(force)
    moment = trsf_s_to_a(moment)

    # -------------------------------#
    # Stress & Strain Field Recovery
    # -------------------------------#
    if voigt_convention == "anba":     
        # Stress field [s_xx, s_yy, s_zz, s_yz, s_xz, s_xy] in voigt notation
        anba.stress_field(force, moment, reference="global", voigt_convention=voigt_convention)  # get stress field in global sys
        tmp_StressF_vec = np.array(anba.STRESS.vector().vec()).reshape(n_el,6)   # global stress field in voight notation
        tmp_StressF = np.array([voight_to_2dtensor(v) for v in tmp_StressF_vec]) #in 2d-tensor notation
        tmp_StressF_tran = np.array([np.dot(np.dot(T.T, a), T) for a in tmp_StressF])
        
        anba.stress_field(force, moment, reference="local", voigt_convention=voigt_convention)  # get stress field in local sys (material coordinates)
        tmp_StressF_M_vec = np.array(anba.STRESS.vector().vec()).reshape(n_el,6)    # local stress field
        tmp_StressF_M = np.array([voight_to_2dtensor(v) for v in tmp_StressF_M_vec]) #in 2d-tensor notation
        tmp_StressF_M_tran = np.array([np.dot(np.dot(T.T, a), T) for a in tmp_StressF_M])
       
        # Strain field in voigt notation
        anba.strain_field(force, moment, reference="global", voigt_convention=voigt_convention)  # get strain field in global sys
        tmp_StrainF_vec = np.array(anba.STRAIN.vector().vec()).reshape(n_el,6)    # global strain field
        tmp_StrainF = np.array([voight_to_2dtensor(v) for v in tmp_StrainF_vec]) #in 2d-tensor notation
        tmp_StrainF_tran = np.array([np.dot(np.dot(T.T, a), T) for a in tmp_StrainF])
        
        anba.strain_field(force, moment, reference="global", voigt_convention=voigt_convention)  # get strain field in global sys
        tmp_StrainF_n_vec = np.array(anba.STRAIN.compute_vertex_values()).reshape(6,n_nodes).T    # global strain field
        tmp_StrainF_n = np.array([voight_to_2dtensor(v) for v in tmp_StrainF_n_vec]) #in 2d-tensor notation
        tmp_StrainF_n_tran = np.array([np.dot(np.dot(T.T, a), T) for a in tmp_StrainF_n])
        
        anba.strain_field(force, moment, reference="local", voigt_convention=voigt_convention)  # get strain field in local sys (material coordinates)
        tmp_StrainF_M_vec = np.array(anba.STRAIN.vector().vec()).reshape(n_el,6)   # local strain field
        tmp_StrainF_M = np.array([voight_to_2dtensor(v) for v in tmp_StrainF_M_vec]) #in 2d-tensor notation
        tmp_StrainF_M_tran = np.array([np.dot(np.dot(T.T, a), T) for a in tmp_StrainF_M])
        
        

    elif voigt_convention == "paraview":  # different ordering compared to "anba"; "paraview" ordering: [s_xx, s_yy, s_zz, s_xy, s_yz, s_xz]
        print("ToDo - Process to paraview output")

        # Export to Paraview format (to be tested!)
        # file_res = do.XDMFFile('output_filename.xdmf')
        # file_res.parameters['functions_share_mesh'] = True
        # file_res.parameters['rewrite_function_mesh'] = False
        # file_res.parameters["flush_output"] = True
        # file_res.write(anba.STRESS, t=2)  # t=unique_number

    # ------------------------- #
    # Warping displacement field
    # ------------------------- #
    tmp = anba.UL.compute_vertex_values()
    arr_ul = tmp.reshape(7,n_nodes)[0:3,:].T
    arr_ul_trans = np.array([np.dot(T.T, a) for a in arr_ul])
    #print(np.allclose(arr_ul_trans, tmp_UL))
    
    # ------------------------- #
    # Warping Gradient field grad(u)
    # ------------------------- #
    
    """ x1_anba = x2_sonata
        x2_anba = x3_sonata
        x3_anba = x1_sonata"""
    
    tmp = anba.ULP.compute_vertex_values()
    arr_ulp = tmp.reshape(7,n_nodes)[0:3,:].T
    #print(elements)
    x,y = (nodes[:, 0], nodes[:, 1])
    u,v,w = (arr_ul[:,0], arr_ul[:,1], arr_ul[:,2])
    triang = tri.Triangulation(x, y) #triangles=elements option sho
    #triang = tri.Triangulation(x, y, triangles=elements) 
    
    min_circle_ratio = .01
    mask = tri.TriAnalyzer(triang).get_flat_tri_mask(min_circle_ratio)
    if np.any(mask):
        triang.set_mask(mask)
    
    xy = np.dstack((triang.x[triang.triangles], triang.y[triang.triangles]))  # shape (ntri,3,2)
    twice_area = np.cross(xy[:,1,:] - xy[:,0,:], xy[:,2,:] - xy[:,0,:])
    mask = twice_area < 1e-10  # shape (ntri)

    if np.any(mask):
        triang.set_mask(mask)

    # #-----------------------------------------------------------------------------
    # # Plotting
    # #-----------------------------------------------------------------------------
    # Set up the figure
    # fig, axs = plt.subplots(nrows=3, ncols=2, constrained_layout=True)
    # axs = axs.flatten()
    
    # # Plot the triangulation.
    # trc = axs[0].tripcolor(triang, w, edgecolors='k', vmin=-1e-5, vmax=1e-5)
    # #axs[0].triplot(triang, ".-", lw=0.4, color="black", alpha=0.3)
    # axs[0].set_title(r'$U_s$, $W_A$')
    # axs[0].set_aspect("equal")
    
    # trc = axs[1].tripcolor(triang, u, edgecolors='k', vmin=-1e-5, vmax=1e-5)
    # #axs[1].triplot(triang, ".-", lw=0.4, color="black", alpha=0.3)
    # axs[1].set_title(r'$V_s$')
    # axs[1].set_aspect("equal")
    
    # trc = axs[2].tripcolor(triang, v, edgecolors='k', vmin=-1e-5, vmax=1e-5)
    # #axs[2].triplot(triang, ".-", lw=0.4, color="black", alpha=0.3)
    # axs[2].set_title(r'$W_s$')
    # axs[2].set_aspect("equal")
    # fig.colorbar(trc, ax=axs[0:2], orientation="horizontal")
    
    #-----------------------------------------------------------------------------
    # Computes the  gradient here at the mesh nodes but could be anywhere else:
    #-----------------------------------------------------------------------------
    tci = tri.LinearTriInterpolator (triang, u)
    (du_dx, du_dy) = tci.gradient(triang.x, triang.y)
    tci = tri.LinearTriInterpolator (triang, v)
    (dv_dx, dv_dy) = tci.gradient(triang.x, triang.y)
    tci = tri.LinearTriInterpolator (triang, w)
    (dw_dx, dw_dy) = tci.gradient(triang.x, triang.y)
    
    du_dz = arr_ulp[:,0]
    dv_dz = arr_ulp[:,1]
    dw_dz = arr_ulp[:,2]
    
    #Partial_derivatives_of_the_warping_field
    # grad_u = np.array([[np.asarray(du_dx), np.asarray(du_dy), np.asarray(du_dz)],
    #                     [np.asarray(dv_dx), np.asarray(dv_dy), np.asarray(dv_dz)],
    #                     [np.asarray(dw_dx), np.asarray(dw_dy), np.asarray(dw_dz)]])
    # grad_u = np.moveaxis(grad_u,2,0)
    # grad_u_trans2 = np.asarray([np.dot(np.dot(T.T,a),T) for a in grad_u])
        
    #In SONATA coordinates
    grad_u_trans = np.array([[np.asarray(dw_dz), np.asarray(dw_dx), np.asarray(dw_dy)],
                             [np.asarray(du_dz), np.asarray(du_dx), np.asarray(du_dy)],
                             [np.asarray(dv_dz), np.asarray(dv_dx), np.asarray(dv_dy)]])
    
    grad_u_trans = np.moveaxis(grad_u_trans,2,0)
    
    
    # # Plot the triangulation.
    # trc = axs[4].tripcolor(triang, dw_dx, edgecolors='k', vmin=-2e-4, vmax=2e-4)
    # #axs[0].triplot(triang, ".-", lw=0.4, color="black", alpha=0.3)
    # axs[4].set_title('dWx_dy')
    # axs[4].set_aspect("equal")
    # fig.colorbar(trc, ax=axs[4], orientation="horizontal")
    
    # trc = axs[5].tripcolor(triang, u, edgecolors='k', vmin=-2e-4, vmax=2e-4)
    # #axs[1].triplot(triang, ".-", lw=0.4, color="black", alpha=0.3)
    # axs[5].set_title('dWx_dz')
    # axs[5].set_aspect("equal")
    # fig.colorbar(trc, ax=axs[5], orientation="horizontal")
    # plt.tight_layout()
    # plt.show()  
    

    return tmp_StressF_tran, tmp_StressF_M_tran, tmp_StrainF_tran, tmp_StrainF_M_tran, arr_ul_trans, grad_u_trans, tmp_StrainF_n_tran


def ComputeShearCenter(stiff_matrix):  # shear center equiv. to elastic axes
    K1 = np.array([[stiff_matrix[i, j] for j in range(3)] for i in range(3)])
    K3 = np.array([[stiff_matrix[i, j+3] for j in range(3)] for i in range(3)])
    Y = np.linalg.solve(K1, -K3)
    return [-Y[2, 0], Y[1, 0]]
    # return [-Y[1,2], Y[0,2]]  # when applied before rotating the matrix from anba to vabs/sonata coordinate system

def ComputeTensionCenter(stiff_matrix):  # tension center equiv. to neutral axes
    K1 = np.array([[stiff_matrix[i, j] for j in range(3)] for i in range(3)])
    K3 = np.array([[stiff_matrix[i, j+3] for j in range(3)] for i in range(3)])
    Y = np.linalg.solve(K1, -K3)
    return [Y[0, 2], -Y[0, 1]]
    # return [Y[2,1], -Y[2,0]]  # when applied before rotating the matrix from anba to vabs/sonata coordinate system

def ComputeMassCenter(mass_matrix):
    M1 = np.array([[mass_matrix[i, j] for j in range(3)] for i in range(3)])
    M3 = np.array([[mass_matrix[i, j+3] for j in range(3)] for i in range(3)])
    Y = np.linalg.solve(M1, -M3)
    return [Y[0,2], Y[1,0]]
    # return [Y[2,1], -Y[2,0]]  # when applied before rotating the matrix from anba to vabs/sonata coordinate system

