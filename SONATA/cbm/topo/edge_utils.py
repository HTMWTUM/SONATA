# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 13:23:47 2023

@author: tobias.pflumm
"""
from OCC.Core.ShapeFix import ShapeFix_ShapeTolerance, ShapeFix_Wire, ShapeFix_EdgeConnect
from OCC.Core.ShapeExtend import ShapeExtend_WireData
from OCC.Core.ShapeAnalysis import ShapeAnalysis_Wire, ShapeAnalysis_WireOrder
from OCC.Core.BRepBuilderAPI import BRepBuilderAPI_MakeWire
            
def fix_tolerance(shape, tolerance=1e-5):
    ShapeFix_ShapeTolerance().SetTolerance(shape, tolerance)


def order_edges(EdgeLst):
    """
    sort unordered edges and return the idx of order

    Parameters
    ----------
    EdgeLst : TYPE
        DESCRIPTION.

    Returns
    -------
    order : TYPE
        DESCRIPTION.

    """
    wd = ShapeExtend_WireData()
    for e in EdgeLst: 
        wd.Add(e)  
    sfw = ShapeFix_Wire()
    sfw.Load(wd.Wire())
    sawo = ShapeAnalysis_WireOrder(True, 1e-5)
    sfw.Analyzer().CheckOrder(sawo)
    sawo.SetChains(1e-5)
    sawo.SetKeepLoopsMode(True)
    #print(sawo.IsDone(), sawo.Status(), sawo.NbChains())
    sawo.Perform(True)
    #print(sawo.IsDone(), sawo.Status(), sawo.NbChains())
    sawo.Chain(1)
    order = [sawo.Ordered(i+1) for i in range(sawo.NbEdges())]
    #print(order)
    return order

def connect_edges(EdgeLst):
    """
    Rebuilds edges to connect with new vertices
    

    Parameters
    ----------
    EdgeLst : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """
    

    
    for i,e in enumerate(EdgeLst):  
        sfec = ShapeFix_EdgeConnect()
        sfec.Add(EdgeLst[i-1], EdgeLst[i])
        sfec.Build()
        
    return None


def edgelst_to_wire(EdgeLst):
    """
    

    Parameters
    ----------
    EdgeLst : TYPE
        DESCRIPTION.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    ##### NEW EdgeLst to Wire #####
    order = order_edges(EdgeLst)
    #print(order)
    ordered_edges = []
    for i in order:
        if i < 0:
            ordered_edges.append(EdgeLst[abs(i)-1].Reversed())
        else:
            ordered_edges.append(EdgeLst[abs(i)-1])
    
    #print(ordered_edges)
    wb = BRepBuilderAPI_MakeWire()     
    for i,e in enumerate(ordered_edges):
        wb.Add(e)
        if wb.Error() == 2:
            sfec = ShapeFix_EdgeConnect()
            sfec.Add(ordered_edges[i-1], ordered_edges[i])
            sfec.Build()
            wb.Add(e)
            
       # print(wb.Error())
    wb.Build()
    return wb.Wire()