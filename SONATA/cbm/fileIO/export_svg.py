#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 13 18:06:28 2020

@author: gu32kij
"""
import os

from OCC.Core.gp import gp_Pnt, gp_Dir, gp_Pnt2d
from OCC.Core.Bnd import Bnd_Box2d

from SONATA.cbm.topo.BSplineLst_utils import discretize_BSplineLst
try:
    import svgwrite
    HAVE_SVGWRITE = True
except ImportError:
    HAVE_SVGWRITE = False


def BSplineLst_to_svg(BSplineLst, tol=0.1, unit="m", style="polyline"):
    """
    Returns a svgwrite.Path for the BSPlineLst, and the 2d bounding box

    Parameters
    ----------
    BSplineLst : TYPE
        DESCRIPTION.
    tol : TYPE, optional
        DESCRIPTION. The default is 0.1.
    unit : TYPE, optional
        DESCRIPTION. The default is "m".
    style : TYPE, optional
        DESCRIPTION. The default is "polyline".

    Returns
    -------
    svgwrite.Path
        DESCRIPTION.
    box2d : TYPE
        DESCRIPTION.

    """
            
    points_2d = discretize_BSplineLst(BSplineLst, tol)
    if unit == "m":
        points_2d = points_2d*-1000
    else:
        points_2d = points_2d*-1
    box2d = Bnd_Box2d()

    for point in points_2d:
        x_p = point[0]
        y_p = point[1]
        box2d.Add(gp_Pnt2d(x_p, y_p))

    if style == "polyline":
        return svgwrite.shapes.Polyline(points_2d, fill="none"), box2d
    elif style == "polygon":
        return svgwrite.shapes.Polygon(points_2d, fill="none"), box2d
    else:
        return None

def export_BSplineLst_to_svg(data, filename=None,
                        width=800, height=600, margin_left=10,
                        margin_top=10, export_hidden_edges=True,
                        location=gp_Pnt(0, 0, 0), direction=gp_Dir(1, 1, 1),
                        unit="m", tol=0.1):
    """
    export a single shape to an svg file and/or string.
    shape: the TopoDS_Shape to export
    filename (optional): if provided, save to an svg file
    width, height (optional): integers, specify the canva size in pixels
    margin_left, margin_top (optional): integers, in pixel
    export_hidden_edges (optional): whether or not draw hidden edges using a dashed line
    location (optional): a gp_Pnt, the lookat
    direction (optional): to set up the projector direction
    color (optional), "default to "black".
    line_width (optional, default to 1): an integer
    """


    if not HAVE_SVGWRITE:
        print("svg exporter not available because the svgwrite package is not installed.")
        print("please use '$ conda install -c conda-forge svgwrite'")
        return False

    # compute polylines for all edges
    # we compute a global 2d bounding box as well, to be able to compute
    # the scale factor and translation vector to apply to all 2d edges so that
    # they fit the svg canva
    global_2d_bounding_box = Bnd_Box2d()

    polylines = []
    for (BSplineLst, color, line_width, fill) in data:
        svg_line, edge_box2d = BSplineLst_to_svg(BSplineLst, tol, unit)
        svg_line.stroke(color, width=line_width, linecap="round")
        svg_line.fill(color=fill, opacity=0.5)
        polylines.append(svg_line)    
        global_2d_bounding_box.Add(edge_box2d)
    
    # translate and scale polylines

    # first compute shape translation and scale according to size/margins
    x_min, y_min, x_max, y_max = global_2d_bounding_box.Get()
    bb2d_width = x_max - x_min
    bb2d_height = y_max - y_min

    # build the svg drawing
    dwg = svgwrite.Drawing(filename, (width, height), debug=True)
    # adjust the view box so that the lines fit then svg canvas
    dwg.viewbox(x_min - margin_left, y_min - margin_top,
                bb2d_width + 2 * margin_left, bb2d_height + 2 * margin_top)

    # then adds the polyline to the svg canva
    for polyline in polylines:
        dwg.add(polyline)

    # export to string or file according to the user choice
    if filename is not None:
        dwg.save()
        if not os.path.isfile(filename):
            raise AssertionError("svg export failed")
        print("Shape successfully exported to %s" % filename)
        return True
    return dwg.tostring()